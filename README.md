# Menunjukkan usecase ranking
- Usecase user

| Usecase | Nilai |
| ------ | ------ |
|   Pendaftaran Pengguna Baru |    90    |
|    Login dan Autentikasi |    95    |
|     Penjadwalan Layanan |80        |
| Pemesanan Layanan Transportasi| 93        |
|      Pemesanan Makanan   |      80  |
|       Pembayaran dan Penagihan|      90|
|      Penilaian dan Ulasan   | 70        |
|     Pelacakan Layanan  |85       |
|      Promosi dan Diskon  |    75    |

- Usecase Manajemen Perusahaan

| Usecase | Nilai |
| ------ | ------ |
|      Manajemen Pengemudi  |  90      |
|     Manajemen Layanan   |     85   |
|    Manajemen Pelanggan    |  90      |
|   Manajemen Keuangan     |  80      |


- Usecase Direksi Perusahaan

| Usecase | Nilai |
| ------ | ------ |
|      Perencanaan  |     95   |
|    Pengawasan Kinerja    |    90    |
| Pengembangan & Pemeliharaan|   85     |
|      Keuangan & Investasi  |       90 |

    
# Penerapan SOLID

Contoh penerapan SOLID pada kodingan index.php pada bagian admin.

```php
// Model (PenjemputanModel.php)
class PenjemputanModel
{
    private $conn;

    public function __construct($conn)
    {
        $this->conn = $conn;
    }

    public function getAllRute()
    {
        $sql = "SELECT * FROM rute";
        $query = mysqli_query($this->conn, $sql);
        $results = mysqli_fetch_all($query, MYSQLI_ASSOC);
        return $results;
    }

    public function simpanRute($tanggal, $dari, $tujuan, $harga, $tipeKendaraan, $idKendaraan)
    {
        $sql = "INSERT INTO rute (depart, rute_from, rute_to, price, Tipe_Kendaraan, id_trans)
                VALUES ('$tanggal', '$dari', '$tujuan', $harga, '$tipeKendaraan', '$idKendaraan')";
        $result = mysqli_query($this->conn, $sql);

        if ($result) {
            echo "Data berhasil disimpan.";
        } else {
            echo "Error: " . $sql . "<br>" . mysqli_error($this->conn);
        }
    }

    public function hapusRute($idRute)
    {
        $sql = "DELETE FROM rute WHERE id_rute = $idRute";
        $result = mysqli_query($this->conn, $sql);

        if ($result) {
            echo "Data berhasil dihapus.";
        } else {
            echo "Error: " . $sql . "<br>" . mysqli_error($this->conn);
        }
    }
}

// View (PenjemputanView.php)
class PenjemputanView
{
    public function renderTable($data)
    {
        ?>
        <table class="table table-striped">
            <thead class="thead-dark">
                <tr>
                    <th>Tanggal Pemesanan</th>
                    <th>Dari</th>
                    <th>Tujuan</th>
                    <th>Harga</th>
                    <th>Tipe Kendaraan</th>
                    <th>ID Kendaraan</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($data as $row) { ?>
                    <tr>
                        <td><?= $row['depart']; ?></td>
                        <td><?= $row['rute_from']; ?></td>
                        <td><?= $row['rute_to']; ?></td>
                        <td><?= $row['price']; ?></td>
                        <td><?= $row['Tipe_Kendaraan']; ?></td>
                        <td><?= $row['id_trans']; ?></td>
                        <td>
                            <a href="rute_d.php?id_rute=<?= $row['id_rute']; ?>" onclick="return confirm('Sure, want to delete?');">
                                <button class="red btn waves-effect"><i class="fa fa-trash"></i></button>
                            </a>
                            <a href="rute_u.php?id_rute=<?= $row['id_rute']; ?>">
                                <button class="yellow btn waves-effect"><i class="fa fa-edit"></i></button>
                            </a>
                        </td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
        <?php
    }
}

// Controller (PenjemputanController.php)
class PenjemputanController
{
    private $model;
    private $view;

    public function __construct($model, $view)
    {
        $this->model = $model;
       

 $this->view = $view;
    }

    public function showTable()
    {
        $data = $this->model->getAllRute();
        $this->view->renderTable($data);
    }

    public function simpanData()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $tanggal = $_POST['depart'];
            $dari = $_POST['rute_from'];
            $tujuan = $_POST['rute_to'];
            $harga = $_POST['price'];
            $tipeKendaraan = $_POST['Tipe_Kendaraan'];
            $idKendaraan = $_POST['id_trans'];

            $this->model->simpanRute($tanggal, $dari, $tujuan, $harga, $tipeKendaraan, $idKendaraan);
        }
    }

    public function hapusData()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'GET' && isset($_GET['id_rute'])) {
            $idRute = $_GET['id_rute'];
            $this->model->hapusRute($idRute);
        }
    }
}

// Usage
include "koneksi.php";
include "PenjemputanModel.php";
include "PenjemputanView.php";
include "PenjemputanController.php";

$model = new PenjemputanModel($conn);
$view = new PenjemputanView();
$controller = new PenjemputanController($model, $view);

$controller->simpanData();
$controller->hapusData();
$controller->showTable();
```

Dalam implementasi di atas, SOLID diterapkan dengan memisahkan logika bisnis (Model), tampilan (View), dan pengontrolan (Controller) ke dalam komponen-komponen terpisah. Hal ini memungkinkan setiap komponen untuk berfokus pada tanggung jawabnya masing-masing, memungkinkan perubahan yang lebih mudah dan fleksibel di masa depan.

# Design Pattern

Pada bagian admin, index.php seperti pada penerapan SOLID, kodingan menggunakan design pattern MVC dimana memisahkan logika bisnis aplikasi menjadi tiga komponen utama:

- Model: Bertanggung jawab untuk mengelola data dan logika bisnis. Pada contoh di atas, PenjemputanModel berinteraksi dengan database untuk mengambil dan menyimpan data jadwal penjemputan.

- View: Menampilkan informasi kepada pengguna dan menerima masukan dari pengguna. Pada contoh di atas, PenjemputanView bertanggung jawab untuk merender tabel jadwal penjemputan ke dalam HTML.

- Controller: Menangani interaksi pengguna, memproses masukan pengguna, dan menghubungkan antara Model dan View. Pada contoh di atas, PenjemputanController menangani tindakan pengguna seperti menyimpan dan menghapus data, serta mengatur pemanggilan fungsi pada Model dan View.

Pola desain MVC memisahkan peran dan tanggung jawab masing-masing komponen, sehingga memungkinkan pengembangan yang lebih terstruktur dan fleksibel. Model-View-Controller (MVC) adalah pola desain yang sangat umum digunakan dalam pengembangan aplikasi web, karena memisahkan logika bisnis, tampilan, dan pengontrolan dengan jelas.

# Konektivitas Database
Untuk databasenya saya menggunakan phpmyadmin
    ![images](https://gitlab.com/alyakwardhani28/uas-pbo-alya-kw-1217050008/-/raw/main/images/Screenshot_2023-07-15_212202.png)

Untuk melakukan autentikasi pengguna dengan memeriksa username dan password yang diberikan melalui formulir login dengan data yang ada di tabel "user" dalam database.
Dengan menambahkan langkah-langkah di atas,akan memiliki koneksi database yang aktif yang dapat digunakan untuk menjalankan query autentikasi dan memeriksa apakah pengguna berhasil masuk atau tidak.
# Webservice dan proses CRUD
- Webpage user
    ![images](https://gitlab.com/alyakwardhani28/uas-pbo-alya-kw-1217050008/-/raw/main/images/Screenshot_2023-07-15_214529.png)

- Webpage admin
    ![images](https://gitlab.com/alyakwardhani28/uas-pbo-alya-kw-1217050008/-/raw/main/images/Screenshot_2023-07-16_023551.png)
    
- Webservice 

Terdapat pada proses signin
    ![images](https://gitlab.com/alyakwardhani28/uas-pbo-alya-kw-1217050008/-/raw/main/images/Screenshot_2023-07-15_215217.png)

- Create

Untuk membuat akun baru pada Webservice dapat dengan cara membuat file create.php lalu memasukan id_user, username, password, dan fullname (menggunakan postman untuk testing) 
    
    <?php
    require_once('getdata.php');

    $id_user = $_POST['id_user'];
    $username = $_POST['username'];
    $password = $_POST['password'];
    $fullname = $_POST['fullname'];

    $query = "INSERT INTO user (id_user, username, password, fullname) VALUES ('$id_user', '$username', '$password', '$fullname')";
    $sql = mysqli_query($conn, $query);

    if ($sql) {
        echo json_encode(array('message' => 'Created'));
    } else {
        echo json_encode(array('message' => 'Error'));
    }
    ?>

![images](https://gitlab.com/alyakwardhani28/uas-pbo-alya-kw-1217050008/-/raw/main/images/Screenshot_2023-07-15_220454.png)
![images](https://gitlab.com/alyakwardhani28/uas-pbo-alya-kw-1217050008/-/raw/main/images/Screenshot_2023-07-15_222117.png)

- Update

Mengupdate username berdasarkan password

    <?php
    require_once('getdata.php');
    parse_str(file_get_contents('php://input'), $value);

    $username = $value['username'];
    $password = $value['password'];

    $query = "UPDATE user SET username = ? WHERE password = ?";
    $stmt = mysqli_prepare($conn, $query);
    mysqli_stmt_bind_param($stmt, "ss", $username, $password);
    $result = mysqli_stmt_execute($stmt);

    if ($result) {
        echo json_encode(array('message' => 'Updated'));
    } else {
        echo json_encode(array('message' => 'Error'));
    }
    ?>
![images](https://gitlab.com/alyakwardhani28/uas-pbo-alya-kw-1217050008/-/raw/main/images/Screenshot_2023-07-15_222951.png)
![images](https://gitlab.com/alyakwardhani28/uas-pbo-alya-kw-1217050008/-/raw/main/images/Screenshot_2023-07-15_223053.png)

- Delete

Menghapus username dari 'user'

    <?php
    require_once('getdata.php');
    parse_str(file_get_contents('php://input'), $value);

    $username = $value['username'];

    $query = "DELETE FROM user WHERE username = ?";
    $stmt = mysqli_prepare($conn, $query);
    mysqli_stmt_bind_param($stmt, "s", $username);
    $result = mysqli_stmt_execute($stmt);

    if ($result) {
        echo json_encode(array('message' => 'Deleted'));
    } else {
        echo json_encode(array('message' => 'Error'));
    }
    ?>

![images](https://gitlab.com/alyakwardhani28/uas-pbo-alya-kw-1217050008/-/raw/main/images/Screenshot_2023-07-15_223853.png)
![images](https://gitlab.com/alyakwardhani28/uas-pbo-alya-kw-1217050008/-/raw/main/images/Screenshot_2023-07-16_002334.png)


# GUI

![images](https://gitlab.com/alyakwardhani28/uas-pbo-alya-kw-1217050008/-/raw/main/images/Screenshot_2023-07-16_022743.png)
![images](https://gitlab.com/alyakwardhani28/uas-pbo-alya-kw-1217050008/-/raw/main/images/Screenshot_2023-07-16_022751.png)
GUI admin, terdiri dari komponen-komponen berikut:

Judul Halaman: <title>Ojek Online - Penjemputan</title> menampilkan judul halaman di tab browser.

Header: <h5>Jadwal Penjemputan Ojek Online</h5> menampilkan judul "Jadwal Penjemputan Ojek Online" di bagian atas halaman.

Tabel Jadwal Penjemputan: <table class="table table-striped">...</table> menampilkan data jadwal penjemputan dalam bentuk tabel. Setiap baris pada tabel mencakup kolom-kolom berikut:

Tanggal Pemesanan
Dari
Tujuan
Harga
Tipe Kendaraan
ID Kendaraan
Tombol Action untuk menghapus atau mengedit jadwal penjemputan.
Formulir Penambahan Jadwal Penjemputan:

Tanggal Pemesanan: <input type="date" id="tanggal" name="depart" class="form-control" required>
Dari: <input type="text" id="dari" name="rute_from" class="form-control" required>
Tujuan: <input type="text" id="tujuan" name="rute_to" class="form-control" required>
Harga: <input type="number" id="harga" name="price" class="form-control" required>
Tipe Kendaraan: <input type="text" id="tipe" name="Tipe_Kendaraan" class="form-control" required>
ID Kendaraan: <input type="text" id="id_kendaraan" name="id_trans" class="form-control" required>
Tombol "Simpan" untuk menyimpan jadwal penjemputan.

Tampilan ini memberikan pengguna antarmuka yang memungkinkan mereka untuk melihat, menambahkan, mengedit, dan menghapus jadwal penjemputan ojek online. Data jadwal penjemputan ditampilkan dalam bentuk tabel, sedangkan formulir digunakan untuk menambahkan jadwal penjemputan baru. Tombol "Simpan" pada formulir digunakan untuk menyimpan jadwal penjemputan baru ke dalam sistem. Tombol "Delete" dan "Edit" pada tabel digunakan untuk menghapus atau mengedit jadwal penjemputan yang ada.


# HTTP Connection
1. URL Input:Memasukkan URL tujuan ke dalam bidang input ini. URL ini mewakili alamat tujuan untuk melakukan koneksi HTTP,"http://localhost/Gojek/admin/login.php".


2. HTTP Methods: Terdapat pilihan metode HTTP yang tersedia, seperti GET, POST.


3. Headers: Pengguna dapat menambahkan header tambahan yang akan dikirimkan dalam koneksi HTTP. Header ini berisi informasi tambahan yang dapat digunakan oleh server untuk memproses permintaan dengan benar.

# YT
https://youtu.be/-mcsqHJQbug
